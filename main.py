# Three Mans Morris

# Imports
import os

# Starting Variables
moving = False
restart = 0
turn = 0
player = 'X'
tile_bag = [1, 2, 3, 4, 5, 6, 7, 8, 9]
choices = [1, 2, 3, 4, 5, 6, 7, 8, 9, 'Restart', 'Exit']

'''
A continue function to save lines later on that will just allow
the user to continue after an error in the tile input
'''


def con():
    input('Press \'Enter\' To Continue ')
    if os.name == 'posix':
        os.system('clear')
    else:
        os.system('cls')

'''
Cleaning the screen after every move to make it look nicer
(Windows and Linux (posix) support)
'''


def clean():
    if os.name == 'posix':
        os.system('clear')
    else:
        os.system('cls')

# Printing the board off, along with a side board with just numbers


def print_board():
    clean()
    num = -1
    print('3-mans-morris\n')
    for i in range(3):
        num += 3
        print(tile_bag[num-2], '--', tile_bag[num-1], '--',
              tile_bag[num], '         ', num-1, '--', num, '--', num+1)
        if i < 2:
            print('|', '  ', '|', '  ', '|', '          |    |    |')

# The Win Screen


def win():
    print_board()
    print('\nPlayer \''+player+'\' Wins')
    input('Press \'Enter\' To End ')
    clean()
    exit()

# Checking the board to see if their is a valid winner on the board


def checkwin():
    # Checking for horizontal wins
    num = -1
    for i in range(3):
        num += 3
        if tile_bag[num] == player:
            if tile_bag[num-1] == player:
                if tile_bag[num-2] == player:
                    win()
    # Checking for verticaal wins
    num = -1
    for i in range(3):
        num += 1
        if tile_bag[num] == player:
            if tile_bag[num+3] == player:
                if tile_bag[num+6] == player:
                    win()
    # Checking for left diagonal wins
    if tile_bag[0] == player:
        if tile_bag[4] == player:
            if tile_bag[8] == player:
                win()
    # Checking for right diagonal wins
    if tile_bag[2] == player:
        if tile_bag[4] == player:
            if tile_bag[6] == player:
                win()

clean()

# The Main Game
try:
    while True:
        # Checking if less then 6 moves have been made to play normally
        if turn < 6:
            print_board()
            # Showing users options and alowing them to select a tile
            print('\nChoices:')
            print(choices)
            print('\nPlayer \''+player+'\' Your Turn')
            tile = input('input: ')
        else:
            tile = ''
        try:
            # If its the 6 turn start moving tiles
            if turn == 6:
                moving = True
            try:
                # Allow the user to leave or restart
                if tile[0] == 'E' or tile[0] == 'e':
                    clean()
                    exit()
                if tile[0] == 'R' or tile[0] == 'r':
                    clean()
                    restart = 1
            except IndexError:
                pass
            # Check for a restart
            if restart == 0:
                # Check if playing is normal
                if moving is False:
                    tile = int(tile)
                    # Place the tile if its avalible
                    if tile_bag.__contains__(tile):
                        tile_bag[tile-1] = player
                        try:
                            # Remove the tile from choices
                            choices.remove(tile)
                        except ValueError:
                            pass
                        # Increase turn by 1
                        turn += 1
                        # Check if there is a win
                        checkwin()
                        # Change Players
                        if player == 'X':
                            player = '□'
                        else:
                            player = 'X'
                    # Check if between the playable tiles
                    elif tile < 1 or tile > 9:
                        print('\nMust Be A Number Between 1 And 9')
                        con()
                    else:
                        print('\nNumber Already Used')
                        con()
                else:
                    # Scanning the board for avliable moves
                    choices = []
                    for i in range(10):
                        i -= 1
                        # Chek if the tile is a player
                        if tile_bag[i] == player:
                            # check infront of player
                            if i == 2 or i == 5 or i == 8 or i == -1:
                                pass
                            else:
                                if tile_bag[i+1] == i+2:
                                    choices.append(str(i+1)+'.'+str(i+2))
                            # Check behind player
                            if i == 0 or i == 3 or i == 6:
                                pass
                            else:
                                if tile_bag[i-1] == i:
                                    choices.append(str(i+1)+'.'+str(i))
                            # Check above player
                            if i == 0 or i == 1 or i == 2:
                                pass
                            else:
                                if tile_bag[i-3] == i-2:
                                    choices.append(str(i+1)+'.'+str(i-2))
                            # Check bellow player
                            if i == 6 or i == 7 or i == 8 or i == -1:
                                pass
                            else:
                                if tile_bag[i+3] == i+4:
                                    choices.append(str(i+1)+'.'+str(i+4))
                    # Same interface as before
                    choices.append('Restart')
                    choices.append('Exit')
                    print_board()
                    print('\nChoices:')
                    print(choices)
                    print('\nPlayer \''+player+'\' Your Turn')
                    tile = input('input: ')
                    try:
                        if tile[0] == 'E' or tile[0] == 'e':
                            clean()
                            exit()
                        if tile[0] == 'R' or tile[0] == 'r':
                            clean()
                            restart = 1
                    except IndexError:
                        pass
                    try:
                        # Move tile if its an option
                        float(tile)
                        if choices.__contains__(tile):
                            tile_bag[int(tile[0])-1] = int(tile[0])
                            tile_bag[int(tile[-1])-1] = player
                            checkwin()
                            if player == 'X':
                                player = '□'
                            else:
                                player = 'X'
                        else:
                            print('\nNumber Must Be In Choices')
                            con()
                    except ValueError:
                        if restart == 0:
                            print('\nMust Be A Number, \'Exit\' or \'Restart\'')
                            con()
            else:
                # Restarting the game
                moving = False
                restart = 0
                tile = 0
                turn = 0
                player = 'X'
                tile_bag = [1, 2, 3, 4, 5, 6, 7, 8, 9]
                choices = [1, 2, 3, 4, 5, 6, 7, 8, 9, 'Restart', 'Exit']
                print_board()
        except ValueError:
            print('\nMust Be A Number, \'Exit\' or \'Restart\'')
            con()
# Making forced closed errors nicer
except (EOFError, KeyboardInterrupt):
    clean()
